// Package mqtt implements home-assistant compatible MQTT sensor devices.
package mqtt

// Sensor represents a home-assistant MQTT sensor.
type Sensor struct {
	ConfigTopic         string `json:"-"`
	StateTopic          string `json:"state_topic"`
	Name                string `json:"name"`
	Qos                 int    `json:"qos"`
	UnitOfMeasurement   string `json:"unit_of_measurement"`
	Icon                string `json:"icon"`
	ExpireAfter         string `json:"expire_after"`
	ValueTemplate       string `json:"value_template"`
	ForceUpdate         bool   `json:"force_update"`
	AvailabilityTopic   string `json:"availability_topic"`
	PayloadAvailable    string `json:"payload_available"`
	PayloadNotAvailable string `json:"payload_not_available"`
	UniqueID            string `json:"unique_id"`
	DeviceClass         string `json:"device_class"`
}

// NewSensor creates a new home-assistant MQTT sensor device.
func NewSensor(configTopic, stateTopic, class, uniqueID, name, unitOfMeasurement, valueTemplate string) *Sensor {
	sensor := new(Sensor)
	sensor.DeviceClass = class
	sensor.Name = name
	sensor.UniqueID = uniqueID
	sensor.ConfigTopic = configTopic
	sensor.StateTopic = stateTopic
	sensor.UnitOfMeasurement = unitOfMeasurement
	sensor.ValueTemplate = valueTemplate
	return sensor
}
