// Package mqtt implements a home-assistant MQTT compatible sensor state payload.
package mqtt

// State holds the data for a homeassistant MQTT sensor state update.
type State struct {
	Topic            string  `json:"-"`
	Temperature      float64 `json:"temperature,omitempty"`
	Humidity         float64 `json:"humidity,omitempty"`
	PressureRelative float64 `json:"pressure_relative,omitempty"`
	PressureAbsolute float64 `json:"pressure_absolute,omitempty"`
	RainRate         float64 `json:"rate,omitempty"`
	EventRain        float64 `json:"event,omitempty"`
	HourlyRain       float64 `json:"hourly,omitempty"`
	DailyRain        float64 `json:"daily,omitempty"`
	WeeklyRain       float64 `json:"weekly,omitempty"`
	MonthlyRain      float64 `json:"monthly,omitempty"`
	YearlyRain       float64 `json:"yearly,omitempty"`
	TotalRain        float64 `json:"totalrain,omitempty"`
	SoilMoisture     float64 `json:"soilmoisture,omitempty"`
}

// NewState creates a new home-assistant MQTT state instance.
func NewState() *State {
	s := new(State)
	return s
}
